from sqlalchemy import ForeignKey, Column, Integer, String, Text
from sqlalchemy.orm import relationship, backref
from helpers.db import Base, Session
from slugify import slugify

class Post(Base):
    # table
    __tablename__ = "posts"
    # type
    id = Column(Integer, primary_key=True)
    title = Column(String)
    body = Column(Text)
    url = Column(String)
    # relationship
    comments = relationship("Comment")

    query = Session.query_property()

    def __init__(self, title, body):
        self.title = title
        self.body = body
        self.url = slugify(title)

    def modify(self, modifications):
        for attr, val in modifications.items():
            if hasattr(self, attr):
                setattr(self, attr, val)

    def add_to_db(self):
        # Session.add(self)
        pass

    def save(self):
        if not self.id:
            Session.add(self)

        Session.commit()

    def new_comment(self, comment):
        self.comments.append(comment)

    def with_id(_id):
        return Post.query.filter(Post.id==_id).first()

    def with_slug(slug):
        return Post.query.filter(Post.url==slug).first()
