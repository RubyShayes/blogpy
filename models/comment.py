from sqlalchemy import ForeignKey, Column, Integer, String, Text
from sqlalchemy.orm import relationship, backref
from helpers.db import Session, Base

class Comment(Base):
    # table
    __tablename__ = "comments"
    # type
    id = Column(Integer, primary_key=True)
    email = Column(String)
    author = Column(String)
    body = Column(Text)
    # relationships
    post_id = Column(Integer, ForeignKey('posts.id'))

    query = Session.query_property()

    def __init__(self, email, author, body, post_id):
        self.email = email
        self.author = author
        self.body = body
        self.post_id
