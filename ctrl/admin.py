from bottle import get, post, put, delete, request, redirect, response
from models.post import Post
from models.comment import Comment
from helpers.db import Session
from helpers.views import render_template

@get("/admin")
def admin():
    posts = Post.query.all()
    return render_template("admin/posts.html").render(posts=posts)

@get("/admin/new")
def show_new_form():
    return render_template("admin/new.html").render()

@post("/admin/new")
def create_post():
    post_title = request.forms.get("title")
    post_body = request.forms.get("body")

    post = Post(title=post_title, body=post_body)
    post.save()

    redirect("/admin")

@get("/admin/edit/<id:int>")
def edit_post(id):
    post = Post.with_id(id)
    return render_template("admin/edit.html").render(post=post)

@post("/admin/edit/<id:int>")
def save_post(id):
    post_title = request.forms.get("title")
    post_body = request.forms.get("body")

    post = Post.with_id(id)
    post.modify({"title": post_title, "body": post_body})
    post.save()

    redirect("/admin")

@post("/admin/delete/<id:int>")
def delete_post(id):
    post = Post.with_id(id)
    Session.delete(post)
    Session.commit()

    redirect("/admin")
