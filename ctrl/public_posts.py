from bottle import get, post, put, delete, request, redirect
from models.post import Post
from models.comment import Comment
from helpers.views import render_template

@get("/")
def index():
    posts = Post.query.all()
    return render_template("index.html").render(posts=posts)

@get("/post/<post_slug>")
def single_post(post_slug):
    post = Post.with_slug(post_slug)
    return render_template("single.html").render(post=post, comments=post.comments)

@post("/post/<post_slug>/comments")
def create_comment(post_slug):
    form_email = request.forms.get("email")
    form_name = request.forms.get("name")
    form_body = request.forms.get("body")

    post = Post.with_slug(post_slug)
    comment = Comment(email=form_email, author=form_name, body=form_body, post_id=post.id)
    post.new_comment(comment)
    post.save()

    redirect("/post/" + post_slug)
