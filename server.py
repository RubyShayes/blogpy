from bottle import run, hook
from helpers.db import Session
import ctrl.public_posts
import ctrl.admin

@hook('after_request')
def release_session_resources():
    Session.remove()

run(host="localhost", port=8000)
