from helpers.db import session, engine, Base, db
from models.post import Post
from models.comment import Comment

post = Post(title="Hello World", body="Just a post to say hello to the world")
post1 = Post(title="Well, hello there!", body="An awesome post.")
post2 = Post(title="Post away!!!", body="An even more awesome post, imagine that")
db.add(post)
db.add(post1)
db.add(post2)

db.commit()
