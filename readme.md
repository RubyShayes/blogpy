### Python Libraries (Pip)
* bottle
* Jinja2
* python-slugify
* SQLAlchemy

### Setup
* run `python migrations.py`
* run `python seed_db.py`

### Run server
* run `python server.py`

### Issues?
* while Bottle has been configured to run `Session.remove()` after every request to free up resources used in querying the database, it is noted that action such as `delete` have a longer processing time.

### Side Notes:
* Authentication was removed because of inconsistencies with the `Bottle.py` micro-framework and the working version (python 3.4).
* With the removal of authentication, the `User` model was also removed.
* To reduce the number of functions where `Session` had to be explicitly passed in, `sqlalchemy`'s `Session.query_property()` and `scoped_session` have been implemented.