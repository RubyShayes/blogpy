from helpers.db import engine, Base
from models.post import Post
from models.comment import Comment

# create database and tables based on metadata of all classes
Base.metadata.create_all(engine)
