from jinja2 import Environment, FileSystemLoader

def render_template(file_name):
    env = Environment(loader=FileSystemLoader('views'))
    template = env.get_template(file_name)
    return template
