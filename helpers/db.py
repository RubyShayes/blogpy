# Session Configuration
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

# From docs: scoped_session usage
# http://docs.sqlalchemy.org/en/rel_0_9/orm/session.html#contextual-thread-local-sessions
# If we call upon the registry a second time, we get back the same Session.
# This pattern allows disparate sections of the application to call upon
# a global scoped_session, so that all those areas may share the same
# session without the need to pass it explicitly.
engine = create_engine("sqlite:///database.db")
Session = scoped_session(sessionmaker(bind=engine))

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
